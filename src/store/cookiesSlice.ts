import { StateCreator } from 'zustand';

export interface CookiesSlice {
	cookiesAccepted: boolean;
	setCookiesAccepted: (payload: boolean) => void;
}

export const createCookiesSlice: StateCreator<
	CookiesSlice,
	[],
	[],
	CookiesSlice
> = (set) => ({
	cookiesAccepted: false,
	setCookiesAccepted: (payload) => set((_) => ({ cookiesAccepted: payload })),
});
