import { totemsData } from '@/lib/totems';
import { StateCreator } from 'zustand';
import { NavigationSlice } from './navigationSlice';

export interface IInteraction {
	id: string;
	position?: { top: number; left: number };
}

export interface InteractionSlice {
	currentInteraction: string | null;
	setCurrentInteraction: (payload: string | null) => void;
	interactions: Array<IInteraction>;
	setInteractions: (payload: IInteraction[]) => void;
}

export const createInteractionSlice: StateCreator<
	InteractionSlice & NavigationSlice,
	[],
	[],
	InteractionSlice
> = (set) => ({
	currentInteraction: null,
	setCurrentInteraction: (payload) =>
		set((_) => ({ currentInteraction: payload })),
	interactions: totemsData.map((value) => ({
		id: value.id,
	})),
	setInteractions: (payload) => set((_) => ({ interactions: payload })),
});
