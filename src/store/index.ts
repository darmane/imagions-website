import { create } from 'zustand';
import { CookiesSlice, createCookiesSlice } from './cookiesSlice';
import { createNavigationSlice, NavigationSlice } from './navigationSlice';
import { createInteractionSlice, InteractionSlice } from './interactionSlice';
import { devtools, persist } from 'zustand/middleware';

const useBoundStore = create<
	CookiesSlice & NavigationSlice & InteractionSlice
>()(
	devtools(
		persist(
			(...a) => ({
				...createCookiesSlice(...a),
				...createNavigationSlice(...a),
				...createInteractionSlice(...a),
			}),
			{
				name: 'cookies-store',
				partialize: (state) => ({
					cookiesAccepted: state.cookiesAccepted,
				}),
			}
		)
	)
);

export default useBoundStore;
