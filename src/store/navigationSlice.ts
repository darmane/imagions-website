import { StateCreator } from 'zustand';

type NavStatus = 'loading' | 'welcome' | 'navigating';

export interface NavigationSlice {
	status: NavStatus;
	setStatus: (payload: NavStatus) => void;
}

export const createNavigationSlice: StateCreator<
	NavigationSlice,
	[],
	[],
	NavigationSlice
> = (set) => ({
	status: 'loading',
	setStatus: (payload) => set((_) => ({ status: payload })),
});
