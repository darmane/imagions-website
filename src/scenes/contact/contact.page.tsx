import { useState } from 'react';
import { NextPageWithLayout } from '@/pages/_app';
import { RoomLayout } from '@/components/layouts';
import Button from '@/components/button';
import Link from 'next/link';
import { NextSeo, NextSeoProps } from 'next-seo';
import { useRouter } from 'next/router';

interface FormData {
	name: string;
	business?: string;
	phone?: string;
	email: string;
	description: string;
}

const defaultState: FormData = {
	name: '',
	email: '',
	description: '',
};

const SEO: NextSeoProps = {
	title: 'Contacto',
	description:
		'Ofrecemos disponibilidad y flexibilidad a nuestra cartera de clientes. ¿Sevilla? ¿Madrid? ¿Valencia? ¿Barcelona? Allí estaremos. Además, los proyectos que hemos realizado como productora audiovisual nos han llevado a lugares como Emiratos, Arabia Saudí, Estados Unidos y diferentes lugares de Europa. ¡Estamos a solo un billete de avión!',
	canonical: 'https://imagions.com/contact',
};

const Contact: NextPageWithLayout = () => {
	const router = useRouter();

	const [data, setData] = useState<FormData>(defaultState);

	const handleOnChangeInput = (id: string, event: any) => {
		setData({ ...data, [id]: event.currentTarget.value });
	};

	const handleOnSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		if (data.name && data.email && data.description) {
			try {
				const response = await fetch('/api/contact', {
					method: 'POST',
					headers: {
						Accept: 'application/json, text/plain, */*',
						'Content-Type': 'application/json',
					},
					body: JSON.stringify(data),
				});

				if (response.ok) {
					setData(defaultState);
					router.replace('/');
				} else {
					throw new Error(
						'Send email failed, with status: ' + response.status
					);
				}
			} catch (error) {
				console.log(error);
			}
		} else {
			console.log('FORM NOT VALID');
		}
	};

	return (
		<>
			<NextSeo {...SEO} />
			<div className='fixed inset-0 flex h-full w-full items-center justify-center p-4'>
				<div className='popup h-full w-full md:max-h-[36rem] md:max-w-[64rem]'>
					<div className='flex h-full w-full flex-col gap-8 border-2 border-solid border-imagions-purple bg-imagions-dark bg-opacity-90 p-4 md:p-8'>
						<h1 className='hidden'>Formulario de contacto</h1>
						<form
							onSubmit={handleOnSubmit}
							className='flex h-full w-full flex-col gap-6 overflow-hidden md:relative md:gap-8'
						>
							<div
								className='mt-4 h-36 w-full bg-contain bg-center bg-no-repeat md:absolute md:top-0 md:right-0 md:m-0 md:h-2/3 md:w-1/2 md:bg-[length:75%]'
								style={{
									backgroundImage:
										'url(/images/logo-imagions-gris-claro.png)',
								}}
							/>
							<div className='flex flex-grow flex-col gap-4 overflow-y-auto'>
								<div className='text-imagions-light'>
									<label
										htmlFor='name'
										className='mb-2 block uppercase leading-none text-imagions-lime'
									>
										<span className='font-bold'>
											NOMBRE Y APELLIDOS
										</span>
										<span>&nbsp;/ NAME AND SURNAME</span>*
									</label>
									<input
										onChange={(event) =>
											handleOnChangeInput('name', event)
										}
										type='text'
										id='name'
										maxLength={64}
										required
										className='w-full border border-solid border-imagions-lime bg-transparent px-2 py-1 md:w-1/2'
									/>
								</div>
								<div className='text-imagions-light'>
									<label
										htmlFor='business'
										className='mb-2 block uppercase leading-none text-imagions-lime'
									>
										<span className='font-bold'>
											NOMBRE DE LA EMPRESA
										</span>
										<span>&nbsp;/ COMPANY NAME</span>
									</label>
									<input
										onChange={(event) =>
											handleOnChangeInput(
												'business',
												event
											)
										}
										type='text'
										id='business'
										maxLength={64}
										className='w-full border border-solid border-imagions-lime bg-transparent px-2 py-1 md:w-1/2'
									/>
								</div>
								<div className='text-imagions-light'>
									<label
										htmlFor='phone'
										className='mb-2 block uppercase leading-none text-imagions-lime'
									>
										<span className='font-bold'>
											TELÉFONO DE CONTACTO
										</span>
										<span>&nbsp;/ CONTACT NUMBER</span>
									</label>
									<input
										onChange={(event) =>
											handleOnChangeInput('phone', event)
										}
										type='tel'
										id='phone'
										maxLength={64}
										className='w-full border border-solid border-imagions-lime bg-transparent px-2 py-1 md:w-1/2'
									/>
								</div>
								<div className='text-imagions-light'>
									<label
										htmlFor='email'
										className='mb-2 block uppercase leading-none text-imagions-lime'
									>
										<span className='font-bold'>
											CORREO ELECTRÓNICO
										</span>
										<span>&nbsp;/ E-MAIL</span>*
									</label>
									<input
										onChange={(event) =>
											handleOnChangeInput('email', event)
										}
										type='email'
										id='email'
										maxLength={64}
										required
										className='w-full border border-solid border-imagions-lime bg-transparent px-2 py-1 md:w-1/2'
									/>
								</div>
								<div className='flex flex-grow flex-col text-imagions-light'>
									<label
										htmlFor='description'
										className='mb-2 block uppercase leading-none text-imagions-lime'
									>
										<span className='font-bold'>
											DESCRIPCIÓN DEL PROYECTO
										</span>
										<span>&nbsp;/ PROJECT DESCRIPTION</span>
										*
									</label>
									<textarea
										onChange={(event) =>
											handleOnChangeInput(
												'description',
												event
											)
										}
										id='description'
										name='description'
										maxLength={512}
										required
										className='h-full min-h-[5rem] w-full resize-none border border-solid border-imagions-lime bg-transparent px-2 py-1'
									/>
								</div>
							</div>
							<div className='flex gap-4 self-center md:self-end'>
								<Link href='/'>
									<Button>
										<span className='block font-bold leading-none'>
											Cancelar
										</span>
										<span className='block leading-none'>
											Cancel
										</span>
									</Button>
								</Link>
								<Button type='submit'>
									<span className='block font-bold leading-none'>
										Enviar
									</span>
									<span className='block leading-none'>
										Send
									</span>
								</Button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</>
	);
};

Contact.getLayout = (page: React.ReactElement) => {
	return <RoomLayout>{page}</RoomLayout>;
};

export default Contact;
