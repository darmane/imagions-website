import { GetStaticPaths, GetStaticProps } from 'next';
import { totemsData } from '@/lib/totems';
import { Props } from './totem.page';

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
	const pid = params?.pid as string;
	const totem = totemsData.find((totem) => totem.id === pid)!;

	return {
		props: {
			totem: totem,
		},
	};
};

export const getStaticPaths: GetStaticPaths = async () => {
	return {
		paths: totemsData.map((totem) => ({
			params: {
				pid: totem.id,
			},
		})),
		fallback: false,
	};
};
