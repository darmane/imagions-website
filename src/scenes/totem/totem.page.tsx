/* eslint-disable @next/next/no-img-element */

import { NextPageWithLayout } from '@/pages/_app';
import { RoomLayout } from '@/components/layouts';
import Button from '@/components/button';
import Link from 'next/link';
import { ITotem } from '@/lib/totems';
import { NextSeo, NextSeoProps } from 'next-seo';

const Totem: NextPageWithLayout<Props> = ({ totem }) => {
	const SEO: NextSeoProps = {
		title: totem.title.es,
		description: totem.description.es,
		canonical: `https://imagions.com/totem/${totem.id}`,
	};

	return (
		<>
			<NextSeo {...SEO} />
			<div className='fixed inset-0 flex h-full w-full items-center justify-center p-4'>
				<div className='popup h-full w-full md:max-h-[36rem] md:max-w-[64rem]'>
					<div className='flex h-full w-full flex-col gap-8 border-2 border-solid border-imagions-purple bg-imagions-dark bg-opacity-90 p-4 md:p-8'>
						{/* MOBILE */}
						<div className='flex flex-grow flex-col overflow-hidden md:hidden'>
							<div
								className='h-60 w-full bg-contain bg-center bg-no-repeat'
								style={{
									backgroundImage: `url(${totem.image})`,
								}}
							/>
							<div className='flex flex-grow flex-col gap-4 overflow-y-auto'>
								<div className='whitespace-pre-line text-imagions-light'>
									<h1 className='text-xl font-bold'>
										{totem.title.es}
									</h1>
									<p>{totem.description.es}</p>
								</div>
								<div className='whitespace-pre-line text-imagions-light'>
									<h2 className='text-xl font-bold'>
										{totem.title.en}
									</h2>
									<p>{totem.description.en}</p>
								</div>
							</div>
						</div>
						{/* DESKTOP */}
						<div className='hidden h-full w-full grid-cols-6 grid-rows-1 overflow-hidden md:grid'>
							<div
								className='col-span-4 col-start-1 row-span-full row-start-1 bg-contain bg-no-repeat'
								style={{
									backgroundImage: `url(${totem.image})`,
									backgroundPosition: '10% center',
								}}
							></div>
							<div className='col-span-full col-start-4 row-span-full row-start-1'>
								<div className='flex h-full flex-col'>
									<div className='flex-grow'></div>
									<div className='flex h-fit flex-col gap-4 overflow-y-auto'>
										<div className='whitespace-pre-line text-imagions-light'>
											<h1 className='text-2xl font-bold'>
												{totem.title.es}
											</h1>
											<p>{totem.description.es}</p>
										</div>
										<div className='whitespace-pre-line text-imagions-light'>
											<h2 className='text-2xl font-bold'>
												{totem.title.en}
											</h2>
											<p>{totem.description.en}</p>
										</div>
									</div>
									<div className='flex-grow'></div>
								</div>
							</div>
						</div>
						{/* BUTTONS */}
						<div className='flex justify-center gap-4 md:justify-end'>
							<Link href='/contact'>
								<Button style={{ height: '100%' }}>
									<div className='px-2'>
										<img
											src='/icons/correo-electronico-imagions-web.png'
											alt='correo-electronico-imagions-web'
											width='32px'
										/>
									</div>
								</Button>
							</Link>
							<Link href='/'>
								<Button>
									<span className='block font-bold leading-none'>
										Sigue explorando
									</span>
									<span className='block leading-none'>
										Keep exploring
									</span>
								</Button>
							</Link>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

Totem.getLayout = (page: React.ReactElement) => {
	return <RoomLayout>{page}</RoomLayout>;
};

export interface Props {
	totem: ITotem;
}

export default Totem;
