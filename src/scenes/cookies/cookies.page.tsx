import { NextPageWithLayout } from '@/pages/_app';
import Link from 'next/link';
import Button from '@/components/button';
import RootLayout from '@/components/layouts';
import { NextSeo, NextSeoProps } from 'next-seo';

const SEO: NextSeoProps = {
	title: 'Cookies',
	description:
		'Una cookie o galleta informática es un pequeño archivo de información que se guarda en su navegador cada vez que visita nuestra página web. Las cookies no suelen recoger categorías especiales de datos personales (datos sensibles). Los datos que guardan son de carácter técnico, preferencias personales, personalización de contenidos, etc.',
	canonical: 'https://imagions.com/cookies',
};

const Cookies: NextPageWithLayout = () => {
	return (
		<>
			<NextSeo {...SEO} />
			<div className='bg-imagions-dark p-4'>
				<div className='container mx-auto'>
					<section className='mb-8 text-imagions-light'>
						<h1 className='mb-6 text-4xl'>Política de Cookies</h1>
						<div className='flex flex-col gap-4'>
							<p>
								Las cookies de este sitio web se usan para
								personalizar el contenido y los anuncios,
								ofrecer funciones de redes sociales y analizar
								el tráfico. Además, compartimos información
								sobre el uso que haga del sitio web con nuestros
								partners de redes sociales, publicidad y
								análisis web, quienes pueden combinarla con otra
								información que les haya proporcionado o que
								hayan recopilado a partir del uso que haya hecho
								de sus servicios.
							</p>
							<p>
								Las cookies son pequeños archivos de texto que
								las páginas web pueden utilizar para hacer más
								eficiente la experiencia del usuario. La ley
								afirma que podemos almacenar cookies en su
								dispositivo si son estrictamente necesarias para
								el funcionamiento de esta página. Para todos los
								demás tipos de cookies necesitamos su permiso.
							</p>
							<p>
								Esta página utiliza tipos diferentes de cookies.
								Algunas cookies son colocadas por servicios de
								terceros que aparecen en nuestras páginas.
							</p>
							<p>
								Las cookies necesarias ayudan a hacer una página
								web utilizable activando funciones básicas como
								la navegación en la página y el acceso a áreas
								seguras de la página web. La página web no puede
								funcionar adecuadamente sin estas cookies.
							</p>
							<p>
								Las cookies de preferencias permiten a la página
								web recordar información que cambia la forma en
								que la página se comporta o el aspecto que
								tiene, como su idioma preferido o la región en
								la que usted se encuentra.
							</p>
							<p>
								Las cookies estadísticas ayudan a los
								propietarios de páginas web a comprender cómo
								interactúan los visitantes con las páginas web
								reuniendo y proporcionando información de forma
								anónima.
							</p>
							<p>
								Las cookies de marketing se utilizan para
								rastrear a los visitantes en las páginas web. La
								intención es mostrar anuncios relevantes y
								atractivos para el usuario individual, y por lo
								tanto, más valiosos para los editores y terceros
								anunciantes.
							</p>
						</div>
					</section>
					<section className='mb-8 text-imagions-gray'>
						<h2 className='mb-6 text-4xl'>Cookies policy</h2>
						<div className='flex flex-col gap-4'>
							<p>
								Las cookies de este sitio web se usan para
								personalizar el contenido y los anuncios,
								ofrecer funciones de redes sociales y analizar
								el tráfico. Además, compartimos información
								sobre el uso que haga del sitio web con nuestros
								partners de redes sociales, publicidad y
								análisis web, quienes pueden combinarla con otra
								información que les haya proporcionado o que
								hayan recopilado a partir del uso que haya hecho
								de sus servicios.
							</p>
							<p>
								Las cookies son pequeños archivos de texto que
								las páginas web pueden utilizar para hacer más
								eficiente la experiencia del usuario. La ley
								afirma que podemos almacenar cookies en su
								dispositivo si son estrictamente necesarias para
								el funcionamiento de esta página. Para todos los
								demás tipos de cookies necesitamos su permiso.
							</p>
							<p>
								Esta página utiliza tipos diferentes de cookies.
								Algunas cookies son colocadas por servicios de
								terceros que aparecen en nuestras páginas.
							</p>
							<p>
								Las cookies necesarias ayudan a hacer una página
								web utilizable activando funciones básicas como
								la navegación en la página y el acceso a áreas
								seguras de la página web. La página web no puede
								funcionar adecuadamente sin estas cookies.
							</p>
							<p>
								Las cookies de preferencias permiten a la página
								web recordar información que cambia la forma en
								que la página se comporta o el aspecto que
								tiene, como su idioma preferido o la región en
								la que usted se encuentra.
							</p>
							<p>
								Las cookies estadísticas ayudan a los
								propietarios de páginas web a comprender cómo
								interactúan los visitantes con las páginas web
								reuniendo y proporcionando información de forma
								anónima.
							</p>
							<p>
								Las cookies de marketing se utilizan para
								rastrear a los visitantes en las páginas web. La
								intención es mostrar anuncios relevantes y
								atractivos para el usuario individual, y por lo
								tanto, más valiosos para los editores y terceros
								anunciantes.
							</p>
						</div>
					</section>
					<div className='text-center'>
						<Link href='/'>
							<Button>
								<span className='block font-bold leading-none'>
									Volver a la experiencia
								</span>
								<span className='block leading-none'>
									Back to the experience
								</span>
							</Button>
						</Link>
					</div>
				</div>
			</div>
		</>
	);
};

Cookies.getLayout = (page: React.ReactElement) => {
	return <RootLayout>{page}</RootLayout>;
};

export default Cookies;
