import useBoundStore from '@/store/index';
import { IInteraction } from '@/store/interactionSlice';
import Link from 'next/link';
import { AiOutlinePlus } from 'react-icons/ai';

const InteractionButton = ({ id, position }: IInteraction) => {
	return (
		<div className='pointer-events-auto absolute' style={{ ...position }}>
			<Link href={`/totem/${id}`}>
				<div className='relative aspect-square w-12 active:opacity-80 motion-safe:animate-fade-in'>
					{/* Ping */}
					<div className='absolute inset-0 m-auto aspect-square w-1/2 animate-ping rounded-full bg-imagions-light bg-opacity-30'></div>
					{/* Outer circle */}
					<div className='absolute inset-0 m-auto aspect-square w-full rounded-full border border-solid border-imagions-lime bg-imagions-lime bg-opacity-20'></div>
					{/* Inner circle */}
					<div className='absolute inset-0 m-auto flex aspect-square w-1/2 items-center justify-center rounded-full bg-imagions-lime transition-[width] duration-300 ease-in-out hover:w-full active:w-full'>
						<AiOutlinePlus
							size='16px'
							className='text-imagions-darker'
						/>
					</div>
				</div>
			</Link>
		</div>
	);
};

const InteractionButtons = () => {
	const interactions = useBoundStore((state) => state.interactions);

	return (
		<div className='relative h-full w-full'>
			{interactions
				.filter((value) => value.position)
				.map((value) => (
					<InteractionButton key={value.id} {...value} />
				))}
		</div>
	);
};

export default InteractionButtons;
