/* eslint-disable @next/next/no-img-element */

import Link from 'next/link';
import useBoundStore from '@/store/index';

const SocialMediaHUD = () => {
	const status = useBoundStore((state) => state.status);

	return (
		<>
			<div
				className={`fixed top-0 left-0 z-0 m-4 ${
					status === 'loading'
						? 'hidden'
						: 'motion-safe:animate-fade-in'
				}`}
			>
				<img
					className='h-6 md:h-12'
					src='/icons/logo-imagions-horizontal.png'
					alt='logo-imagions-horizontal'
				/>
			</div>
			<div
				className={`pointer-events-auto fixed bottom-0 right-0 z-0 m-4 flex gap-2 ${
					status === 'loading'
						? 'hidden'
						: 'motion-safe:animate-fade-in'
				}`}
			>
				<Link href='/contact' className='mr-1'>
					<img
						className='w-8 md:w-12'
						src='/icons/correo-electronico-imagions-web.png'
						alt='correo-electronico-imagions'
					/>
				</Link>
				<Link
					href='https://www.instagram.com/imagions'
					target='_blank'
					rel='noreferrer noopener'
				>
					<img
						className='w-8 md:w-12'
						src='/icons/instagram-imagions-web.png'
						alt='instagram-imagions'
					/>
				</Link>
				<Link
					href='https://www.youtube.com/channel/UCLck0g80DeCL8bSKly1WkOg'
					target='_blank'
					rel='noreferrer noopener'
				>
					<img
						className='w-8 md:w-12'
						src='/icons/youtube-imagions-web.png'
						alt='youtube-imagions'
					/>
				</Link>
			</div>
		</>
	);
};

export default SocialMediaHUD;
