import { NextPageWithLayout } from '@/pages/_app';
import { RoomLayout } from '@/components/layouts';
import Button from '@/components/button';
import InteractionButtons from './interaction-buttons';
import useBoundStore from '@/store/index';
import { motion } from 'framer-motion';
import SocialMediaHUD from './social-media-hud';

const WelcomePopUp = ({ onClickExplore }: { onClickExplore: () => void }) => {
	return (
		<motion.div
			className='pointer-events-auto flex h-full w-full items-center justify-center p-2'
			initial={{ opacity: 0 }}
			animate={{ opacity: 1 }}
			exit={{ opacity: 0 }}
		>
			<div className='popup'>
				<div className='border-2 border-solid border-imagions-purple bg-imagions-dark bg-opacity-90'>
					<div className='flex max-w-xl flex-col items-center gap-8 px-4 py-8 md:px-8'>
						<div className='flex flex-col gap-4 text-center'>
							<div className='whitespace-pre-line text-imagions-light'>
								<h1 className='text-xl font-bold'>
									Bienvenid@ a imagions
								</h1>
								<p>
									Bienvenid@ a imagions, el lugar donde damos
									vida a tu idea y la convertimos en una
									experiencia única. Disfruta de nuestra
									oficina buscando los tótems que hemos ido
									acumulando durante nuestra historia como
									estudio creativo y productora audiovisual.
								</p>
							</div>
							<div className='whitespace-pre-line text-imagions-light'>
								<h2 className='text-xl font-bold'>
									Welcome to imagions
								</h2>
								<p>
									Welcome to imagions, where we bring your
									ideas to life and turn them into a unique
									experience. As a creative studio and
									audiovisual production company, we have
									built up a portfolio of totems over the
									years. Enjoy our office while you browse
									through them.
								</p>
							</div>
						</div>
						<div>
							<Button onClick={onClickExplore}>
								<span className='block font-bold leading-none'>
									Explorar
								</span>
								<span className='block leading-none'>
									Explore
								</span>
							</Button>
						</div>
					</div>
				</div>
			</div>
		</motion.div>
	);
};

const Home: NextPageWithLayout = () => {
	const status = useBoundStore((state) => state.status);
	const setStatus = useBoundStore((state) => state.setStatus);

	const handleOnClickWelcome = () => setStatus('navigating');

	return (
		<div className='pointer-events-none fixed inset-0 h-full w-full'>
			<h1 className='hidden'>
				imagions es un estudio creativo especializado en la producción
				audiovisual.
			</h1>
			{status === 'loading' ? (
				<></>
			) : status === 'welcome' ? (
				<WelcomePopUp onClickExplore={handleOnClickWelcome} />
			) : (
				<>
					<InteractionButtons />
					<SocialMediaHUD />
				</>
			)}
		</div>
	);
};

Home.getLayout = (page: React.ReactElement) => {
	return <RoomLayout>{page}</RoomLayout>;
};

export default Home;
