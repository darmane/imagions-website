export const totemsData: ITotem[] = [
	// +X
	{
		id: 'popcorn',
		image: '/images/totems/02_imagions_productora_audiovisual_cine_documentales.png',
		coordinates: [49, -17.5, -5.5],
		name: 'Palomitas',
		title: { es: '¡Próximamente en cines!', en: 'Coming soon!' },
		description: {
			es: '¡En imagions nos gusta el cine! Colaboramos con otras productoras en el ámbito de efectos visuales y animación para cine y documentales. Tenemos amplia experiencia en la conceptualización y diseño de cabeceras, grafismos, así como la aplicación de complejas técnicas de VFX para generar escenografías y entornos virtuales 3D en grandes producciones.',
			en: 'At imagions, we love cinema! We partner with other production companies to create visual effects and animation for films and documentaries. We have extensive experience in conceptualizing and designing film titles and motion graphics, in addition to applying complex VFX techniques to generate 3D virtual environments and sets in major productions.',
		},
	},
	// +Z
	{
		id: 'telephone',
		image: '/images/totems/03_imagions_productora_audiovisual_sevilla_contacto_telefono.png',
		coordinates: [22.5, -5.5, 49],
		name: 'Teléfono',
		title: { es: 'Donde nos necesites', en: 'Wherever you need us' },
		description: {
			es: 'Ofrecemos disponibilidad y flexibilidad a nuestra cartera de clientes. ¿Sevilla? ¿Madrid? ¿Valencia? ¿Barcelona? Allí estaremos. Además, los proyectos que hemos realizado como productora audiovisual nos han llevado a lugares como Emiratos, Arabia Saudí, Estados Unidos y diferentes lugares de Europa. ¡Estamos a solo un billete de avión!',
			en: 'We are highly flexible and ready to travel to meet the needs of all our clients. Wherever your project is, we can make it. We have provided audiovisual services throughout Spain and Europe as well as in the United Arab Emirates, Saudi Arabia and the USA. We are only a flight away!',
		},
	},
	{
		id: 'skull',
		image: '/images/totems/04_imagions_productora_audiovisual_museos_dinosaurio.png',
		coordinates: [-5.5, 10, 49],
		name: 'Cráneo',
		title: { es: '¡Eureka!', en: 'Eureka!' },
		description: {
			es: 'Solemos trabajar en proyectos internacionales de carácter científico con un alto nivel de exigencia y rigurosidad. Desde campañas para industrias farmacéuticas hasta museos llenos de fósiles de dinosaurios. Colaboramos con especialistas en cada materia dependiendo de las necesidades de cada proyecto.',
			en: 'We work on highly demanding and precise international scientific projects, ranging from pharmaceutical campaigns to dinosaur fossil exhibitions. Our close cooperation with expert field partners enables us to fully meet the needs of each project.',
		},
	},
	{
		id: 'globe',
		image: '/images/totems/05_imagions_productora_audiovisual_internacional.png',
		coordinates: [-13.5, 5, 49],
		name: 'Globo',
		title: { es: '¿Siguiente parada?', en: 'Next stop?' },
		description: {
			es: 'Estamos acostumbrados a trabajar con clientes en el extranjero, viajes y diferentes husos horarios. Desde Dubai hasta Nueva York, encontramos la manera de adaptarnos a las necesidades del cliente. ¡Nos gusta viajar!',
			en: 'We are well accustomed to working with international customers. We love to travel and are used to working in different time zones. Whether you are in Dubai or New York, we will find a way to adapt to your needs.',
		},
	},
	{
		id: 'chromeball',
		image: '/images/totems/06_imagions_productora_audiovisual_efectosvisuales_chromeball.png',
		coordinates: [-5.5, 0, 49],
		name: 'Bola de cromo',
		title: { es: 'Todo es posible', en: 'Everything is possible' },
		description: {
			es: 'En imagions nos encantan los efectos visuales. Ofrecemos asesoramiento y supervisión en la preproducción y la ejecución de tu proyecto. Producción virtual, matte painting, rotoscopia, limpieza de cromas, diseño de escenarios en 3D, animación e integración de personajes, CGI…',
			en: 'At imagions we love visual effects. We provide consultancy and guidance for project pre-production and execution. We can assist you with virtual production, matte painting, rotoscoping, chroma keying, 3D set design, character animation and integration, CGI and more.',
		},
	},
	{
		id: 'clapperboard',
		image: '/images/totems/07_imagions_productora_audiovisual_cine_documentales_claqueta.png',
		coordinates: [-20.5, -5, 49],
		name: 'Claqueta',
		title: { es: 'Luces… cámara… ¡acción!', en: 'Lights… camera… action!' },
		description: {
			es: 'Luces… cámara… ¡acción!\nCuando contamos historias… ¡el cielo es el límite! Guionización, localización, producción, rodaje y postproducción. Gracias a nuestra amplia red de colaboradores, ¡podemos con todo!',
			en: 'Lights… camera… action!\nWhen it comes to stories, the sky is the limit! From scripting, location and production services to shooting and postproduction. Anything is possible with our extensive network of partners.',
		},
	},
	{
		id: 'canvas',
		image: '/images/totems/09_imagions_estudio_creativo_productora_audiovisual_creatividad_guiones_lienzo.png',
		coordinates: [-5.5, -10, 49],
		name: 'Lienzo',
		title: {
			es: 'Un mundo de posibilidades',
			en: 'A world of possibilities',
		},
		description: {
			es: 'En imagions nos gustan los retos, y, como estudio creativo, no hay mayor reto que un lienzo en blanco. ¿Tienes una idea pero no sabes cómo contarla? Confía en nosotros, ofrecemos asesoramiento y dirección creativa desde el primer momento.',
			en: 'We at imagions enjoy a good challenge, and there is none better for a creative studio than a blank canvas. Do you have an idea but are not sure how to portray it? Trust us to give you creative advice and direction from day one.',
		},
	},
	// -X
	{
		id: 'dynosaur',
		image: '/images/totems/09_imagions_productora_audiovisual_animacion_3d_efectos_visuales_dinosaurio_velociraptor.png',
		coordinates: [-49, -22.5, 12.5],
		name: 'Velociraptor',
		title: { es: 'Oficina pet friendly', en: 'Pet-friendly office' },
		description: {
			es: 'En imagions nos gustan los animales y ¡tenemos contratado a un velociraptor como actor! La animación 3D y los efectos visuales nos flipan. Como productora audiovisual desarrollamos las piezas desde la planificación y preproducción hasta el último píxel y render final.',
			// eslint-disable-next-line quotes
			en: "Animals are a favourite at imagions and we've even hired a velociraptor as an actor! We are crazy about 3D animation and visual effects. As an audiovisual production company, we develop the objects from planning and pre-production to the last pixel and final rendering.",
		},
	},
	// -Z
	{
		id: 'laptop',
		image: '/images/totems/01_imagions_productora_audiovisual_software_.png',
		coordinates: [6, -11, -49],
		name: 'Laptop',
		title: {
			es: 'Postproducción y tecnología',
			en: 'Postproduction and technology',
		},
		description: {
			es: 'En imagions estamos al día de las últimas técnicas de postproducción audiovisual y trabajamos con una gran variedad de softwares para producir diferente tipología de contenidos, adaptados a formatos como videomapping, instalaciones inmersivas o realidad virtual. Desde gráficos o efectos visuales, animación 2D y 3D, motion graphics, contenido interactivo o proyecciones inmersivas complejas, te apoyamos en tu proyecto desde el comienzo con asesoramiento y/o dirección creativa.',
			en: 'At imagions, we use state-of-the-art audiovisual post-production techniques and work with a wide range of software. The content we produce is varied and adapted to formats such as videomapping, immersive installations and virtual reality. Whether you are looking for graphics or visual effects, 2D or 3D animations, motion graphics, interactive content or complex immersive projections, we are here to support your project from the very start with advice and/or creative direction.',
		},
	},
	{
		id: 'logo',
		image: '/images/totems/10_imagions_productora_audiovisual_estudio_creativo_logo.png',
		coordinates: [4.5, 10.5, -49],
		name: 'Nosotros',
		title: { es: 'Nosotros imaginamos', en: 'We imagine' },
		description: {
			es: 'imagions es una libre interpretación del francés “imaginamos”. Queremos ofrecer a nuestros clientes la posibilidad de crear sin límites. Nosotros nos encargamos de dar forma a la idea y llevarla a la realidad, diseñando los procesos necesarios para producir cualquier tipo de contenido audiovisual.',
			en: 'imagions is an adaptation from the French "let\'s imagine". We want to give our clients the opportunity to create without limits. We take care of shaping your ideas and bringing them to life by designing the necessary processes to produce any type of audiovisual content.',
		},
	},
];

export interface ITotem {
	readonly id: string;
	readonly image: string;
	readonly coordinates: [number, number, number];
	readonly name: string;
	readonly title: { es: string; en: string };
	readonly description: { es: string; en: string };
}
