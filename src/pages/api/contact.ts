import { NextApiRequest, NextApiResponse } from 'next';
import * as nodemailer from 'nodemailer';

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
	if (req.method == 'POST') {
		try {
			let transporter = nodemailer.createTransport({
				host: process.env.MAIL_HOST,
				port: process.env.MAIL_PORT as any,
				secure: true, // true for 465, false for other ports
				// tls: {
				// 	ciphers: 'SSLv3', // Only for Outlook.
				// },
				auth: {
					user: process.env.MAIL_USER,
					pass: process.env.MAIL_PASSWORD,
				},
			});

			// Mail to imagions
			await transporter.sendMail({
				from: process.env.MAIL_USER,
				to: process.env.MAIL_TO,
				subject: 'IMAGIONS formulario de contacto',
				text:
					req.body.description +
					' | Sent from: ' +
					req.body.name +
					' | ' +
					req.body.email,
				html: `
                    <div>
                        <h1>Formulario de contacto</h1>
                        <ul>
                            <li>
                                <p>Nombre y apellidos: <spam>${req.body.name}</spam></p>
                            </li>
                            <li>
                                <p>Nombre de la empresa: <spam>${req.body.business}</spam></p>
                            </li>
                            <li>
                                <p>Teléfono: <spam>${req.body.phone}</spam></p>
                            </li>
                            <li>
                                <p>E-Mail: <spam>${req.body.email}</spam></p>
                            </li>
                            <li>
                                <p>Descripción del proyecto:</p>
                                <p>${req.body.description}</p>
                            </li>
                        </ul>
                    </div>
                `,
			});

			// Confirmation email to submitter.
			// Don't need to await because it doesn't matter what happen with this email.
			transporter.sendMail({
				from: process.env.MAIL_USER,
				to: `${req.body.email}`,
				subject: 'IMAGIONS formulario de contacto',
				text: '¡Hemos recibido tu mensaje! Nos pondremos en contacto contigo lo antes posible. \nWe have received your message! We will contact you as soon as possible.',
				html: `
                    <div>
                        <div>
							<h2>¡Hemos recibido tu mensaje!</h2>
							<p>Nos pondremos en contacto contigo lo antes posible.</p>
						</div>
						<div>
							<h2>We have received your message!</h2>
							<p>We will contact you as soon as possible.</p>
						</div>
                    </div>
                `,
			});

			res.status(200).end();
		} catch (error) {
			console.log(error);
			res.status(500).end();
		}
	} else {
		res.status(405).end();
	}
};

export default handler;
