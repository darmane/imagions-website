import { NextPage } from 'next';
import Link from 'next/link';
import Button from '@/components/button';

const Error: NextPage = () => {
	return (
		<div className='flex h-full w-full flex-col items-center justify-center gap-8 bg-imagions-dark p-4'>
			<div className='text-center'>
				<p className='text-imagions-light'>
					Ha habido un error, ¡lo sentimos! Prueba a volver a la
					experiencia.
				</p>
				<p className='text-imagions-gray'>
					There has been an error, sorry! Try going back to the
					experience.
				</p>
			</div>
			<div>
				<Link href='/'>
					<Button>
						<span className='block font-bold leading-none'>
							Volver a la experiencia
						</span>
						<span className='block leading-none'>
							Back to the experience
						</span>
					</Button>
				</Link>
			</div>
		</div>
	);
};

export default Error;
