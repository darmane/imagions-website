import '@/styles/globals.css';
import { NextPage } from 'next';
import { AppProps } from 'next/app';
import { motion, AnimatePresence } from 'framer-motion';
import { GoogleAnalytics } from 'nextjs-google-analytics';
import { DefaultSeo } from 'next-seo';
import SEO from 'next-seo.config';

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
	getLayout?: (page: React.ReactElement) => React.ReactNode;
};

type AppPropsWithLayout = AppProps & {
	Component: NextPageWithLayout;
};

function MyApp({ Component, pageProps, router }: AppPropsWithLayout) {
	const getLayout = Component.getLayout ?? ((page) => page);

	return (
		<>
			<GoogleAnalytics trackPageViews />
			<DefaultSeo {...SEO} />
			{getLayout(
				<AnimatePresence>
					<motion.main
						key={router.route}
						initial={{ opacity: 0 }}
						animate={{ opacity: 1 }}
						exit={{ opacity: 0 }}
						className='overflow-auto'
					>
						<Component {...pageProps} />
					</motion.main>
				</AnimatePresence>
			)}
		</>
	);
}

export default MyApp;
