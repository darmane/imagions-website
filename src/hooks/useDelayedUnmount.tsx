import { useState, useEffect } from 'react';

const useDelayedUnmount = (
	isMounted: boolean,
	delay: number = 500,
	callback: () => void = () => {}
) => {
	const [shouldRender, setShouldRender] = useState(false);

	useEffect(() => {
		let timeoutId: any;

		if (isMounted && !shouldRender) {
			setShouldRender(true);
		} else if (!isMounted && shouldRender) {
			timeoutId = setTimeout(() => {
				callback();
				setShouldRender(false);
			}, delay);
		}

		return () => clearTimeout(timeoutId);

		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isMounted, delay, shouldRender]);

	return shouldRender;
};

export default useDelayedUnmount;
