import { useEffect, useMemo, useRef, useState } from 'react';
import * as THREE from 'three';
// import StatsImpl from 'stats.js';
import { Canvas, useFrame } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import type { OrbitControls as OrbitControlsImpl } from 'three-stdlib';
import useBoundStore from '@/store/index';
import { IInteraction } from '@/store/interactionSlice';
import { totemsData } from '@/lib/totems';
import RootLayout from '@/components/layouts';
import CookiesConsent from '@/components/cookies-consent';
import { useRouter } from 'next/router';

// const Stats = () => {
// 	const [stats] = useState(() => new StatsImpl());

// 	useEffect(() => {
// 		stats.showPanel(0);
// 		document.body.appendChild(stats.dom);

// 		return () => {
// 			document.body.removeChild(stats.dom);
// 		};
// 		// eslint-disable-next-line
// 	}, []);

// 	return useFrame((state) => {
// 		stats.begin();
// 		state.gl.render(state.scene, state.camera);
// 		stats.end();
// 	}, 1);
// };

const Cubemap = () => {
	const setInteractions = useBoundStore((state) => state.setInteractions);

	useFrame((state) => {
		// Get camera state
		const camera = state.camera;
		camera.updateMatrixWorld();

		// Get canvas state
		const { width, height } = state.size;
		const halfWidth = width * 0.5;
		const halfHeight = height * 0.5;

		// Recalculate absolute position of each totem point
		const interactivePoints: IInteraction[] = [];
		totemsData.forEach((totem) => {
			// Project world location to camera plane
			const [x, y, z] = totem.coordinates;
			const worldLocation = new THREE.Vector3(x, y, z);
			const cameraLocation = worldLocation.project(camera);
			// Check if the point it behind the camera
			if (cameraLocation.z < 0 || cameraLocation.z > 1) {
				interactivePoints.push({ id: totem.id });
				return;
			}
			// Translate camera location to screen space
			const screenPosition = {
				top: -1 * cameraLocation.y * halfHeight + halfHeight,
				left: cameraLocation.x * halfWidth + halfWidth,
			};

			interactivePoints.push({
				id: totem.id,
				position: screenPosition,
			});
		});

		setInteractions(interactivePoints);
	});

	const cubeMaterial = useMemo(() => {
		let video: any = document.getElementById('cubemap-texture');
		video.play();

		const texture = new THREE.VideoTexture(video);
		texture.encoding = THREE.sRGBEncoding;
		texture.needsUpdate = true;

		const material = new THREE.MeshBasicMaterial({
			map: texture,
			side: THREE.BackSide,
			toneMapped: false,
		});
		material.needsUpdate = true;

		return material;
	}, []);

	const geometry = useMemo(() => {
		const box = new THREE.BoxGeometry(100, 100, 100);

		var uvAttribute: any = box.getAttribute('uv');
		// -X
		uvAttribute.setXY(0, 0, 0.5);
		uvAttribute.setXY(1, 1 / 3, 0.5);
		uvAttribute.setXY(2, 0, 0);
		uvAttribute.setXY(3, 1 / 3, 0);

		// X
		uvAttribute.setXY(4, 1 / 3, 1);
		uvAttribute.setXY(5, 2 / 3, 1);
		uvAttribute.setXY(6, 1 / 3, 0.5);
		uvAttribute.setXY(7, 2 / 3, 0.5);

		// -Y
		uvAttribute.setXY(8, 2 / 3, 0);
		uvAttribute.setXY(9, 1 / 3, 0);
		uvAttribute.setXY(10, 2 / 3, 0.5);
		uvAttribute.setXY(11, 1 / 3, 0.5);

		// Y
		uvAttribute.setXY(12, 1, 0);
		uvAttribute.setXY(13, 2 / 3, 0);
		uvAttribute.setXY(14, 1, 0.5);
		uvAttribute.setXY(15, 2 / 3, 0.5);

		// -Z
		uvAttribute.setXY(16, 2 / 3, 1);
		uvAttribute.setXY(17, 1, 1);
		uvAttribute.setXY(18, 2 / 3, 0.5);
		uvAttribute.setXY(19, 1, 0.5);

		// Z
		uvAttribute.setXY(20, 0, 1);
		uvAttribute.setXY(21, 1 / 3, 1);
		uvAttribute.setXY(22, 0, 0.5);
		uvAttribute.setXY(23, 1 / 3, 0.5);

		return box;
	}, []);

	return (
		<mesh
			material={cubeMaterial}
			scale={[-1, 1, 1]}
			geometry={geometry}
			position={[0, 0, 0]}
		/>
	);
};

const Room = () => {
	const { pathname } = useRouter();

	// const cameraRef = useRef<THREE.Camera>();
	const controlRef = useRef<OrbitControlsImpl>(null);

	// Global state
	const status = useBoundStore((state) => state.status);

	const passiveSupported = useMemo(() => {
		let passiveIfSupported = false;

		try {
			const options = Object.defineProperty({}, 'passive', {
				get: () => {
					passiveIfSupported = true;
				},
			});

			window.addEventListener('testPassive', null as any, options);
			window.removeEventListener('testPassive', null as any, options);
		} catch (error) {}

		return passiveIfSupported;
	}, []);

	const onMouseWheel = (event: WheelEvent) => {
		!passiveSupported && event.preventDefault();

		if (!controlRef.current?.enableRotate) {
			return;
		}

		if (controlRef.current) {
			const currentAngle = controlRef.current.getAzimuthalAngle();
			controlRef.current.setAzimuthalAngle(
				currentAngle + event.deltaY / 100
			);
		}
	};

	const enableRotate = pathname === '/' && status === 'navigating';

	return (
		<Canvas gl={{ antialias: false, powerPreference: 'high-performance' }}>
			{/* <PerspectiveCamera
				ref={cameraRef}
				position={[0, 1, 0]}
				zoom={0.5}
				name='roomcamera'
				makeDefault
			/> */}
			<OrbitControls
				ref={controlRef}
				// camera={cameraRef.current}
				enableDamping
				enablePan={false}
				enableZoom={false}
				autoRotate={status === 'welcome'}
				autoRotateSpeed={-1}
				enableRotate={enableRotate}
				minPolarAngle={Math.PI / 2}
				maxPolarAngle={Math.PI / 2}
				addEventListener={() =>
					addEventListener(
						'wheel',
						onMouseWheel,
						passiveSupported ? { passive: true } : false
					)
				}
			/>
			<Cubemap />
			{/* <Stats /> */}
		</Canvas>
	);
};

const RoomLayout = ({ children }: Props) => {
	const router = useRouter();

	const videoRef = useRef<HTMLVideoElement>(null);

	// Local state
	const [videoLoaded, setVideoLoaded] = useState(false);

	// Global state
	const status = useBoundStore((state) => state.status);
	const setStatus = useBoundStore((state) => state.setStatus);

	// Load video texture (necessary to trigger the video events)
	useEffect(() => {
		if (videoRef?.current) {
			videoRef.current.load();
		}
	}, [videoRef]);

	useEffect(() => {
		// Redirect to home if loading
		if (
			status !== 'navigating' &&
			(router.pathname === '/contact' ||
				router.pathname.startsWith('/totem/'))
		) {
			router.replace('/');
		}

		// Clear local state
		return () => {
			setVideoLoaded(false);
			setStatus('loading');
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const handleOnLoadedData = () => {
		setVideoLoaded(true);
	};

	const handleTimeUpdate = (
		event: React.SyntheticEvent<HTMLVideoElement>
	) => {
		const duration = event.currentTarget.duration;
		const currentTime = event.currentTarget.currentTime;

		// We consider the scene is loaded after the intro (second 3)
		if (status === 'loading' && currentTime > 3) {
			setStatus('welcome');
		} else if (currentTime >= duration - 0.5) {
			event.currentTarget.currentTime = 3;
		}
	};

	return (
		<RootLayout>
			<>
				<video
					ref={videoRef}
					id='cubemap-texture'
					loop={true}
					muted
					crossOrigin='anonymous'
					playsInline
					hidden
					onTimeUpdate={handleTimeUpdate}
					onLoadedData={handleOnLoadedData}
				>
					{/* WEBM */}
					{/* <source
						src='/videos/imagions_app_BG_4k.webm'
						type='video/webm'
					/> */}
					<source
						src='/videos/imagions_app_BG_2k.webm'
						type='video/webm'
					/>
					<source
						src='/videos/imagions_app_BG_1k.webm'
						type='video/webm'
					/>
					{/* MP4 265 */}
					{/* <source
						src='/videos/imagions_app_BG_4k_h265.mp4'
						type='video/mp4'
					/> */}
					<source
						src='/videos/imagions_app_BG_2k_h265.mp4'
						type='video/mp4'
					/>
					<source
						src='/videos/imagions_app_BG_1k_h265.mp4'
						type='video/mp4'
					/>
					{/* MP4 264 */}
					<source
						src='/videos/imagions_app_BG_2k.mp4'
						type='video/mp4'
					/>
				</video>
				{videoLoaded && (
					<>
						<div className='h-full w-full'>
							<Room />
						</div>
						{children}
						<CookiesConsent />
					</>
				)}
			</>
		</RootLayout>
	);
};

interface Props {
	children: React.ReactElement;
}

export default RoomLayout;
