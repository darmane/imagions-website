export { default as RoomLayout } from './room-layout';

const RootLayout = ({ children }: Props) => {
	return <>{children}</>;
};

interface Props {
	children: React.ReactElement;
}

export default RootLayout;
