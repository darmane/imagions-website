const Button = ({
	type = 'button',
	border = true,
	children,
	onClick,
	style,
}: Props) => {
	return (
		<button
			type={type}
			className={`${
				border ? 'border' : 'border-none'
			} border-solid border-imagions-lime px-2 py-0.5 uppercase text-imagions-lime hover:opacity-80`}
			onClick={onClick}
			style={style}
		>
			{children}
		</button>
	);
};

interface Props {
	type?: 'button' | 'reset' | 'submit';
	border?: boolean;
	children: React.ReactNode;
	onClick?: () => void;
	style?: React.CSSProperties;
}

export default Button;
