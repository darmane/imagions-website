import useBoundStore from '@/store/index';
import Button from '@/components/button';
import { motion, AnimatePresence } from 'framer-motion';
import Link from 'next/link';

const CookiesConsent = () => {
	const status = useBoundStore((state) => state.status);
	const cookiesAccepted = useBoundStore((state) => state.cookiesAccepted);
	const setCookiesAccepted = useBoundStore(
		(state) => state.setCookiesAccepted
	);

	const handleAcceptCookies = () => {
		setCookiesAccepted(true);
	};

	return (
		<AnimatePresence>
			{status === 'navigating' && !cookiesAccepted && (
				<motion.div
					className='fixed bottom-0 w-full'
					initial={{ opacity: 0 }}
					animate={{ opacity: 1 }}
					exit={{ opacity: 0 }}
				>
					<div className='container z-50 mx-auto p-2'>
						<div className='popup'>
							<div className='border-2 border-solid border-imagions-purple bg-imagions-dark bg-opacity-90 p-4'>
								<div className='flex flex-col items-center gap-6'>
									<div className='flex flex-grow flex-col justify-center gap-2 overflow-y-auto text-center'>
										<p className='text-imagions-light'>
											Utilizamos
											<span className='text-imagions-lime'>
												<Link href='/cookies'>
													&nbsp;cookies&nbsp;
												</Link>
											</span>
											para obtener datos estadísticos de
											la navegación de nuestros usuarios y
											mejorar nuestros servicios. Si
											acepta o continúa navegando,
											consideramos que acepta su uso.
										</p>
										<p className='text-imagions-gray'>
											We use
											<span className='text-imagions-lime'>
												<Link href='/cookies'>
													&nbsp;cookies&nbsp;
												</Link>
											</span>
											to obtain statistical data from our
											users&apos; websearch and improve
											our services. If you accept or
											continue browsing, we consider that
											you accept its use.
										</p>
									</div>
									<div>
										<Button onClick={handleAcceptCookies}>
											<span className='block font-bold leading-none'>
												Estoy de acuerdo
											</span>
											<span className='block leading-none'>
												I agree
											</span>
										</Button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</motion.div>
			)}
		</AnimatePresence>
	);
};

export default CookiesConsent;
