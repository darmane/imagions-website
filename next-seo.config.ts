import { NextSeoProps } from 'next-seo';

const SEO: NextSeoProps = {
	defaultTitle: 'imagions - Estudio Creativo / Productora audiovisual',
	titleTemplate: '%s - imagions',
	description:
		'imagions es un estudio creativo especializado en la producción audiovisual. Trabajamos en ámbito nacional e internacional para cine, publicidad, museos, eventos, documentales, instalaciones inmersivas... ¡Contáctanos y pide más información!',
	canonical: 'https://imagions.com/',
	languageAlternates: [
		{
			hrefLang: 'en-US',
			href: 'https://imagions.com',
		},
		{
			hrefLang: 'es-ES',
			href: 'https://imagions.com',
		},
	],
	additionalMetaTags: [
		{
			name: 'author',
			content: 'imagions',
		},
		{
			name: 'author',
			content: 'darmane',
		},
		{
			name: 'twitter:image',
			content: 'https://imagions.com/images/logo-imagions_800x600.png',
		},
		{
			property: 'og:image',
			content: 'https://imagions.com/images/logo-imagions_800x600.png',
		},
	],
	additionalLinkTags: [
		{
			rel: 'icon',
			href: 'https://imagions.com/favicon.ico',
		},
		{
			rel: 'apple-touch-icon',
			href: 'https://imagions.com/apple-touch-icon.png',
			sizes: '180x180',
		},
		{
			rel: 'shortcut icon',
			href: 'https://imagions.com/favicon.ico',
			type: 'image/x-icon',
		},
		{
			rel: 'preload',
			href: 'https://imagions.com/fonts/Avenir-Book.woff2',
			as: 'font',
			type: 'font/woff2',
			crossOrigin: 'anonymous',
		},
		{
			rel: 'preload',
			href: 'https://imagions.com/fonts/Avenir-Medium.woff2',
			as: 'font',
			type: 'font/woff2',
			crossOrigin: 'anonymous',
		},
		{
			rel: 'preload',
			href: 'https://imagions.com/fonts/Avenir-Black.woff2',
			as: 'font',
			type: 'font/woff2',
			crossOrigin: 'anonymous',
		},
	],
	twitter: {
		cardType: 'summary_large_image',
	},
	openGraph: {
		url: 'https://imagions.com',
		type: 'website',
		locale: 'es_ES',
		// eslint-disable-next-line camelcase
		site_name: 'imagions',
		title: 'imagions',
		description:
			'imagions es un estudio creativo especializado en la producción audiovisual. Trabajamos en ámbito nacional e internacional para cine, publicidad, museos, eventos, documentales, instalaciones inmersivas... ¡Contáctanos y pide más información!',
		images: [
			{
				url: 'https://imagions.com/images/logo-imagions_800x600.png',
				width: 800,
				height: 600,
				alt: 'imagions',
				type: 'image/png',
			},
			{
				url: 'https://imagions.com/images/logo-imagions_900x800.png',
				width: 900,
				height: 800,
				alt: 'imagions',
				type: 'image/png',
			},
		],
	},
};

export default SEO;
