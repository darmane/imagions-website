/** @type {import('tailwindcss').Config} */

module.exports = {
	content: [
		'./src/app/**/*.{js,ts,jsx,tsx}',
		'./src/components/**/*.{js,ts,jsx,tsx}',
		'./src/scenes/**/*.{js,ts,jsx,tsx}',
		'./src/pages/**/*.{js,ts,jsx,tsx}',
	],
	theme: {
		extend: {
			fontFamily: {
				sans: ['Avenir', 'sans-serif'], // Default font family
			},
			animation: {
				'fade-in': 'fadeIn 700ms ease-in forwards',
				'fade-out': 'fadeOut 700ms ease-in forwards',
			},
			keyframes: {
				fadeIn: {
					'0%': { opacity: 0 },
					'100%': { opacity: 1 },
				},
				fadeOut: {
					'0%': { opacity: 1 },
					'100%': { opacity: 0 },
				},
			},
			colors: {
				imagions: {
					darker: '#1E1E1E',
					dark: '#333333',
					gray: '#CBCBCB',
					light: '#EDF0F4',
					purple: '#5D2AEB',
					lime: '#DEFB2F',
				},
			},
			variants: {
				animation: ['motion-safe'],
			},
		},
	},
	plugins: [],
};
