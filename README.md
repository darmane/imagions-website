# imagions Website

Website of imagions, an audiovisual production studio.

**[www.imagions.com](https://imagions.com)**

## Getting started
- (Optional) In case you want to use a .env file to set the environment variables, duplicate the file ".env.template" and rename the copy to ".env", then set the values of the environment variables.
- npm run build
- (Development) npm run dev | OR | (Production) npm run start

## Stack
- Next.js: React framework.
- Tailwind CSS: Design and styles.
- Zustand: State management.
- Nodemailer: Backend for the contact form.

## License

MIT license.

**Hey!** Read the [LICENSE.md](LICENSE.md) carefully before doing anything with this project.