/** @type {import('next-sitemap').IConfig} */
module.exports = {
	siteUrl: `https://${process.env.HOSTNAME}`,
	generateRobotsTxt: true,
};
